﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ComboController : MonoBehaviour
{
    [SerializeField] private GameObject _comboTextObject = null;
    [SerializeField] private GameObject _comboRewardTextObject = null;
    [SerializeField] private TextMeshProUGUI _comboText = null;
    [SerializeField] private TextMeshProUGUI _comboRewardText = null;
    [SerializeField] private AudioSource _audioSource = null;
    [SerializeField] private AudioClip  _audioClip = null;

    public void ComboHit()
    {
        _comboTextObject.SetActive(true);
        StartCoroutine(ComboHitRoutine());
    }

    public void HideComboText()
    {
        _comboTextObject.SetActive(false);
    }

    public void SetComboText(int combo)
    {
        _comboText.text = "COMBO X" + combo;
    }

    public bool ComboTextVisible()
    {
        return _comboTextObject.activeSelf;
    }

    public void SetRewardText(int combo)
    {
        _comboRewardTextObject.SetActive(true);
        _comboRewardText.text = "+" + combo;
        StartCoroutine(RewardFadeOut());
    }

    IEnumerator RewardFadeOut()
    {
        _audioSource.PlayOneShot(_audioClip);
        while (_comboRewardText.color.a > 0)
        {
            _comboRewardText.color -= new Color(0, 0, 0, 1.5f * Time.deltaTime);
            yield return null;
        }
        _comboRewardText.color += new Color(0, 0, 0, 1);
        _comboRewardTextObject.SetActive(false);
    }

    IEnumerator ComboHitRoutine()
    {
        for (float f = 0f; f <= .3; f += .1f)
        {
            _comboText.transform.localScale += new Vector3(.1f, .1f, .1f);
            yield return null;
        }

        for (float f = 0f; f <= .3; f += .1f)
        {
            _comboText.transform.localScale -= new Vector3(.1f, .1f, .1f);
            yield return null;
        }
    }
}
