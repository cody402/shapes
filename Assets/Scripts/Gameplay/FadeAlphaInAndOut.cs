﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeAlphaInAndOut : MonoBehaviour
{
    [SerializeField] private Image _image = null;
    [SerializeField] private Color _alarmColor;

    private float _currentFill = 0f;
    private bool _growing = true;
    private float _maxFill = .5f;

    private void Update()
    {

        if (_growing) {
            _currentFill = Mathf.Clamp(_currentFill, 0, _maxFill) + Time.deltaTime;
            _alarmColor.a = Mathf.Clamp(_currentFill / _maxFill, 0f, .5f);
            _image.color = _alarmColor;
            if (_image.color.a >= .48f)
            {
                _growing = false;
            }
        }

        if (!_growing)
        {
            _currentFill = Mathf.Clamp(_currentFill, 0, _maxFill) - Time.deltaTime;
            _alarmColor.a = Mathf.Clamp(_currentFill / _maxFill, 0f, .5f);
            _image.color = _alarmColor;
            if (_image.color.a <= .05)
            {
                _growing = true;
            }
        }
    }
}
