﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveUpAndDown : MonoBehaviour
{
    private float _startingY;

    private void Start()
    {
        _startingY = transform.position.y;
    }

    private void Update()
    {
        transform.position = new Vector3(transform.position.x, Mathf.PingPong(Time.time, 1) + _startingY, transform.position.z);
    }
}
