﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicController : MonoBehaviour
{
    [SerializeField] AudioSource _audioSource = null;
    [SerializeField] AudioClip _menuMusic = null;
    [SerializeField] AudioClip _gameMusic = null;

    public void PlayMenuMusic()
    {
        PlaySong(_menuMusic);
    }

    public void PlayGameMusic()
    {
        PlaySong(_gameMusic);
    }

    public void PlaySong(AudioClip song)
    {
        _audioSource.Stop();
        _audioSource.clip = song;
        _audioSource.loop = true;
        _audioSource.Play();
    }
}
