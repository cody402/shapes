﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Volume_Controller : MonoBehaviour
{
    [SerializeField] private Slider _sfx_slider;
    [SerializeField] private Slider _music_slider;
    [SerializeField] private AudioSource _music_source;
    [SerializeField] private AudioSource _sfx_source;
    [SerializeField] private AudioSource _sfx_game_source;

    private void Start()
    {
        _music_source.volume = PlayerPrefs.GetFloat("music", 1f);
        _sfx_source.volume = PlayerPrefs.GetFloat("sfx", 1f);
        _sfx_game_source.volume = PlayerPrefs.GetFloat("sfx", 1f);
        _music_slider.value = PlayerPrefs.GetFloat("music", 1f);
        _sfx_slider.value = PlayerPrefs.GetFloat("sfx", 1f);

        _sfx_slider.onValueChanged.AddListener(delegate { ValueChangeCheck(); });
        _music_slider.onValueChanged.AddListener(delegate { ValueChangeCheck(); });
    }

    public void ValueChangeCheck()
    {
        _music_source.volume = _music_slider.value;
        _sfx_source.volume = _sfx_slider.value;
        _sfx_game_source.volume = _sfx_slider.value;
        PlayerPrefs.SetFloat("music", _music_slider.value);
        PlayerPrefs.SetFloat("sfx", _sfx_slider.value);
    }
}
