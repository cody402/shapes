﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarScaler : MonoBehaviour
{
    [SerializeField] private Image _image = null;
    [SerializeField] private GameController _gameController = null;
    [SerializeField] private AudioSource _audioSource = null;
    [SerializeField] private AudioClip _alarmSound = null;
    [SerializeField] private GameObject _alarmFader = null;

    private Color _startingColor;
    private float _maxFill = 10f;
    private float _currentFill= 10f;
    private float _triggerTime = 0;
    private float _timeOffset = 1.25f;

    private void Start()
    {
        _startingColor = _image.color;
    }

    public float GetMaxFill()
    {
        return _maxFill;
    }

    public void SetMaxFill(float fill)
    {
        _maxFill = fill;
    }

    private void Update()
    {
        if (!_gameController.GetPause())
        {
            _maxFill = Mathf.Clamp(_maxFill - (.2f * Time.deltaTime), 1f, 10f);
            _currentFill = Mathf.Clamp(_currentFill,0,_maxFill) - 1 * Time.deltaTime;
            _image.fillAmount = Mathf.Clamp(_currentFill / _maxFill, 0f, 1f);
          
            if(_currentFill <= _maxFill * .5f)
            {
                if (!_audioSource.isPlaying)
                {
                    _triggerTime = Time.time + _timeOffset;
                    _audioSource.clip = _alarmSound;
                    _audioSource.loop = true;
                    _audioSource.Play();
                    _alarmFader.SetActive(true);
                    _image.color = Color.red;
                }
            }
            else
            {
                if(_triggerTime < Time.time)
                {
                _image.color = _startingColor;
                _alarmFader.SetActive(false);
                _audioSource.Stop();
                }
            }

            if (_currentFill <= 0)
            {
                _image.color = _startingColor;
                _alarmFader.SetActive(false);
                _gameController.GameOver();
                _audioSource.Stop();
                _maxFill = 10f;
                _currentFill = _maxFill;
            }
        }
    }

    public void AddTime(float time)
    {
        _currentFill += time;
    }

    public void SubtractTime(float time)
    {
        _currentFill -= time;
    }
}
