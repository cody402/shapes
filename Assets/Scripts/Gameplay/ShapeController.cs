﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShapeController : MonoBehaviour
{
    [SerializeField] private GameController _gameController = null;
    [SerializeField] private SpriteRenderer _spriteRenderer = null;
    [SerializeField] private bool _centerShape = false;
    [SerializeField] private Transform _startingPosition;
    [SerializeField] private Transform[] _positions;
    private Vector3 _currentPosition;
    private int _id;
    private Color32 _color;
    private bool _rotate = false;

    private float _changeTime;
    private float _changeOffset = 10;
    private bool _changeDirection;

    private void Awake()
    {
        if (!_centerShape)
        {
            _currentPosition = _startingPosition.position;
        }
    }

    public void SetShapeRotation(bool rotate)
    {
        _rotate = rotate;
        _changeTime = Time.time + _changeOffset;
    }

    public void ResetRotation()
    {
        transform.localEulerAngles = new Vector3(0, 0, 0);
    }

    public void Update()
    {
        if (_rotate && !_centerShape)
        {
            if(Time.time > _changeTime)
            {
                _changeDirection = !_changeDirection;
                _changeTime = Time.time + Random.Range(3,_changeOffset);
            }
            if (_changeDirection)
            {
                transform.localEulerAngles += new Vector3(0, 0, 1f);
            }
            else
            {
                transform.localEulerAngles -= new Vector3(0, 0, 1f);
            }
        }
    }

    public void ResetPosition()
    {
        _currentPosition = _startingPosition.position;
        transform.position = _startingPosition.position;
    }

    public void StartMoving(bool direction)
    {
        GoToPosition(direction);
    }

    IEnumerator SmoothMoveRoutine(Transform startMarker, Transform endMarker)
    {
        var duration = 1f;
        float journey = 0f;
        while (journey <= duration)
        {
            journey = journey + Time.deltaTime;
            float percent = Mathf.Clamp01(journey / duration);
            transform.position = Vector3.Lerp(startMarker.position, endMarker.position, percent);
            yield return null;
        }
    }

    public void SetId(int id)
    {
        _id = id;
    }

    public void GoToPosition(bool forward)
    {
        if (forward)
        {
            // Move forward
            for (var x = 0; x < _positions.Length; x++)
            {
                if (_currentPosition == _positions[x].position)
                {
                    if (x + 1 >= _positions.Length)
                    {
                        StartCoroutine(SmoothMoveRoutine(transform, _positions[0]));
                        _currentPosition = _positions[0].position;
                        break;
                    }
                    else
                    {
                        StartCoroutine(SmoothMoveRoutine(transform, _positions[x + 1]));
                        _currentPosition = _positions[x + 1].position;

                        break;
                    }
                }
            }
        }
        else
        {
            // Move back
            for (var x = 0; x < _positions.Length; x++)
            {
                if (_currentPosition == _positions[x].position)
                {
                    if (x - 1 < 0)
                    {
                        StartCoroutine(SmoothMoveRoutine(transform, _positions[7]));
                        _currentPosition = _positions[7].position;
                        break;
                    }
                    else
                    {
                        StartCoroutine(SmoothMoveRoutine(transform, _positions[x - 1]));
                        _currentPosition = _positions[x - 1].position;

                        break;
                    }
                }
            }
        }
    }

    public void SetShape(Sprite shape)
    {
        _spriteRenderer.sprite = shape;
    }

    public void SetColor(Color32 color)
    {
        _spriteRenderer.color = color;
    }

    public int GetId()
    {
        return _id;
    }

    void OnMouseDown()
    {
        if (!_centerShape)
        {
            if (_id == _gameController.GetPrimaryID())
            {
                _gameController.Hit();
            }
            else
            {
                _gameController.Miss();
            }
        }
    }
}
