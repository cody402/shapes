﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameController : MonoBehaviour
{
    [SerializeField] private ShapeController _shapeCenter = null;
    [SerializeField] private ShapeController _shapeLeft = null;
    [SerializeField] private ShapeController _shapeRight = null;
    [SerializeField] private ShapeController _shapeTop = null;
    [SerializeField] private ShapeController _shapeBottom = null;
    [SerializeField] private UIController _uiController = null;
    [SerializeField] private AudioSource _audioSource = null;
    [SerializeField] private AudioClip _scoreSound = null;
    [SerializeField] private AudioClip _missSound = null;
    [SerializeField] private AudioClip _moveSound = null;
    [SerializeField] private BarScaler _barScaler = null;
    [SerializeField] private Sprite[] _shapeSprites = null;
    [SerializeField] private ComboController _comboController = null;
    [SerializeField] private AudioClip _comboSound = null;
    [SerializeField] private GameObject _tutorialPointer = null;
    [SerializeField] private GameObject _tutorialtext = null;
    [SerializeField] private DataManager _dataManager = null;

    private bool _moving = false;
    private bool _rotating = false;
    private float _comboOffset = .75f;
    private float _lastHit;
    private int _currentCombo = 0;
    private bool _paused;
    private int _score = 0;
    private ShapeController[] _shapeControllers;
    private bool _showTutorial = true;
    private bool _continueFromLastGame = false;

    private int[] _ids = new int[] { 1, 2, 3, 4 };
    private Color32[] _colors;

    private void Start()
    {
        _lastHit -= _comboOffset;
        _shapeControllers = new ShapeController[] { _shapeLeft, _shapeRight, _shapeTop, _shapeBottom };
        _colors = new Color32[] {new Color32(246, 81, 29, 255), new Color32(255, 180, 0, 255), new Color32(0, 166, 237, 255), new Color32(127, 184, 0, 255) };
        if (_dataManager.LoadTurotialFromPlayerPrefs() == 0)
        {
            SetTutorial();
        }
        else
        {
            _showTutorial = false;
            _tutorialPointer.SetActive(false);
            _tutorialtext.SetActive(false);
            ResetIds();
        }
    }

 
    public void NewGame(int score, float maxFill, bool continueGame)
    {
        if (continueGame)
        {
            _continueFromLastGame = true;
        }
        else
        {
            _continueFromLastGame = false;
        }

        if (_continueFromLastGame)
        {
            if(maxFill <= 0)
            {
                maxFill = 10;
            }
            _score = score;
            _uiController.SetScoreText("Score: " + _score);
            _barScaler.SetMaxFill(maxFill);
            _uiController.SetAdPreviouslyShown(true);
        }
        else
        {
            _uiController.SetAdPreviouslyShown(false);
        }
    }

    public void SetTutorial()
    {
        _paused = true;
        _shapeCenter.SetId(2);
        _shapeCenter.SetColor(_colors[0]);
        _shapeCenter.SetShape(_shapeSprites[2]);
        _shapeControllers[0].SetId(1);
        _shapeControllers[0].SetColor(_colors[0]);
        _shapeControllers[0].SetShape(_shapeSprites[0]);
        _shapeControllers[1].SetId(4);
        _shapeControllers[1].SetColor(_colors[1]);
        _shapeControllers[1].SetShape(_shapeSprites[1]);
        _shapeControllers[2].SetId(3);
        _shapeControllers[2].SetColor(_colors[3]);
        _shapeControllers[2].SetShape(_shapeSprites[3]);
        _shapeControllers[3].SetId(2);
        _shapeControllers[3].SetColor(_colors[2]);
        _shapeControllers[3].SetShape(_shapeSprites[2]);
    }

    IEnumerator MovePositionRoutine()
    {
        var x = 0;
        bool direction = (Random.value > 0.5f);
        while (true)
        {
            if (x == Random.Range(3, 10) || x > 10)
            {
                direction = (Random.value > 0.5f);
                x = 0;
            }
            foreach (ShapeController shape in _shapeControllers)
            {
                _audioSource.PlayOneShot(_moveSound);
                shape.GoToPosition(direction);
            }
            x++;
            yield return new WaitForSeconds(Random.Range(1,3));
        }

    }

    private void Update()
    {
        if(_comboController.ComboTextVisible() && !(Time.time <= (_lastHit + _comboOffset)))
        {
            {
                _score += _currentCombo;
                _uiController.SetScoreText("Score: " + _score);
                _comboController.SetRewardText(_currentCombo);
                _comboController.SetComboText(_currentCombo);
            }
            _currentCombo = 0;
            _comboController.HideComboText();
        }
    }

    public void SetPause(bool paused)
    {
        _paused = paused;
    }

    public bool GetPause()
    {
        return _paused;
    }

    public void Miss()
    {
        if (!_showTutorial)
        {
            ResetIds();
            _barScaler.SubtractTime(1.5f);
            _currentCombo = 0;
            _comboController.HideComboText();
            _audioSource.PlayOneShot(_missSound);
        }
    }

    public void Hit()
    {
        if (_showTutorial)
        {
            _paused = false;
            _tutorialPointer.SetActive(false);
            _tutorialtext.SetActive(false);
            _dataManager.SaveTutorialPlayerPrefs(1);
            _showTutorial = false;
        }
        if (Time.time <= (_lastHit + _comboOffset))
        {
            _currentCombo++;
            _comboController.ComboHit();
            _comboController.SetComboText(_currentCombo);
            _audioSource.PlayOneShot(_comboSound);
        }
        _lastHit = Time.time;
        _score++;
        if(_score > 50 && !_moving)
        {
            _moving = true;
            StartCoroutine(MovePositionRoutine());
        }
        if (_score > 100 && !_rotating)
        {
            foreach (ShapeController shape in _shapeControllers)
            {
                shape.SetShapeRotation(true);
            }
        }
        _barScaler.AddTime(1f);
        _uiController.SetScoreText("Score: " + _score);
        _audioSource.PlayOneShot(_scoreSound);
        ResetIds();
    }

    public void GameOver()
    {
        SetPause(true);
        _moving = false;
        StopCoroutine(MovePositionRoutine());
        foreach(ShapeController shape in _shapeControllers)
        {
            shape.ResetPosition();
            shape.SetShapeRotation(false);
            shape.ResetRotation();
        }
        _uiController.ShowGameOverScreen(_score, _barScaler.GetMaxFill());
        _score = 0;
        _uiController.SetScoreText("Score: " + _score);

    }

    public int GetPrimaryID()
    {
        return _shapeCenter.GetId();
    }

    private int[] ShuffleIds(int[] ids)
    {
        for (int i = 0; i < ids.Length; i++)
        {
            int rnd = Random.Range(1, ids.Length);
            var temp = ids[rnd];
            ids[rnd] = ids[i];
            ids[i] = temp;
        }     
        return ids;
    }

    // Reset Logic
    private void ResetIds()
    {
        ResetIdForShapeMatching();
    }

    private void ResetIdForShapeMatching()
    {
        var primaryId = Random.Range(1, 4);
        _shapeCenter.SetId(primaryId);
        _shapeCenter.SetShape(_shapeSprites[primaryId - 1]);
        _ids = ShuffleIds(_ids);
        var i = 0;
        foreach (ShapeController shape in _shapeControllers)
        {
            shape.SetId(_ids[i]);
            shape.SetShape(_shapeSprites[_ids[i] - 1]);
            i++;
        }

        var pId = Random.Range(1, 4);
        _shapeCenter.SetColor(_colors[pId]);
        _ids = ShuffleIds(_ids);
        var x = 0;
        foreach (ShapeController shape in _shapeControllers)
        {
            shape.SetColor(_colors[_ids[x] - 1]);
            x++;
        }
    }

    private void ResetIdForColorMatching()
    {
        var primaryId = Random.Range(1, 4);
        _shapeCenter.SetId(primaryId);
        _shapeCenter.SetColor(_colors[primaryId - 1]);
        _ids = ShuffleIds(_ids);
        var i = 0;
        foreach (ShapeController shape in _shapeControllers)
        {
            shape.SetId(_ids[i]);
            shape.SetColor(_colors[_ids[i] - 1]);
            i++;
        }

        var pId = Random.Range(1, 4);
        _shapeCenter.SetShape(_shapeSprites[pId]);
        _ids = ShuffleIds(_ids);
        var x = 0;
        foreach (ShapeController shape in _shapeControllers)
        {
            shape.SetShape(_shapeSprites[_ids[x] - 1]);
            x++;
        }
    }
}
