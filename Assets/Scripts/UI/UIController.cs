﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

using GooglePlayGames;
using UnityEngine.SocialPlatforms;
using GooglePlayGames.BasicApi;

public class UIController : MonoBehaviour
{
    [SerializeField] private GameObject _menuParent = null;
    [SerializeField] private TextMeshProUGUI _scoreText = null;
    [SerializeField] private TextMeshProUGUI _comboText = null;
    [SerializeField] private TextMeshProUGUI _highScoreText = null;
    [SerializeField] private TextMeshProUGUI _gameOverScoreText = null;
    [SerializeField] private GameController _gameController = null;
    [SerializeField] private DataManager _dataManager = null;

    // Screen
    [SerializeField] private GameObject _startMenu = null;
    [SerializeField] private GameObject _optionsMenu = null;
    [SerializeField] private GameObject _gameScreen = null;
    [SerializeField] private GameObject _gameOverMenu = null;
    [SerializeField] private GameObject _adScreen = null;

    // Sound
    [SerializeField] private AudioSource _audioScource = null;
    [SerializeField] private MusicController _musicController = null;
    [SerializeField] private AudioClip _selectSound = null;
    [SerializeField] private AudioClip _gameOverSound = null;

    // Button
    [SerializeField] private Button _backButton = null;

    private int _highScore;
    private GameObject _prevScreen;
    private float _maxFill;
    private int _score;
    private bool _continueGame;
    private bool _adPreviouslyShown;

    private void Start()
    {
        _prevScreen = _startMenu;
        _gameController.SetPause(true);
    }

    public void RefreshScreen()
    {
        _startMenu.SetActive(false);
        _optionsMenu.SetActive(false);
        _gameScreen.SetActive(false);
        _gameOverMenu.SetActive(false);
        _adScreen.SetActive(false);
    }

    public void ShowStartMenu()
    {
        PlayMenuMusic();
        RefreshScreen();
        PlaySelectSound();
        _startMenu.SetActive(true);
        _prevScreen = _startMenu;
    }

    private void SetHighScore(int highScore)
    {
        _highScore = highScore;
    }

    private int GetHighScore()
    {
        return _highScore;
    }

    public void ShowGameOverScreen(int score, float maxFill)
    {
        PlayMenuMusic();
        if (PlayGamesPlatform.Instance.IsAuthenticated())
        {
            PlayGamesPlatform.Instance.LoadScores(
            "CgkI5-6CjOMQEAIQAA",
            LeaderboardStart.PlayerCentered,
            1,
            LeaderboardCollection.Public,
            LeaderboardTimeSpan.AllTime,
        (LeaderboardScoreData data) =>
        {
            SetHighScore((int)data.PlayerScore.value);
            if(GetHighScore() < _dataManager.LoadScoreFromPlayerPrefs())
            {
                SetHighScore(_dataManager.LoadScoreFromPlayerPrefs());
            }
        });
        }
        else
        {
            SetHighScore(_dataManager.LoadScoreFromPlayerPrefs());
        }

        if (score > GetHighScore())
        {
            _highScoreText.text = "High Score: " + score;
            _dataManager.SaveScoreToPlayerPrefs(score);

            if (PlayGamesPlatform.Instance.IsAuthenticated())
            {
                // Save to play services, create make some type of sucess or error message.
                Social.ReportScore(score, "CgkI5-6CjOMQEAIQAA", (bool success) => { });
            }
        }
        else
        {
            _highScoreText.text = "High Score: " + GetHighScore();
        }
        _maxFill = maxFill;
        _score = score;
        _gameOverScoreText.text = "Score: " + score;
        _menuParent.SetActive(true);
        RefreshScreen();
        _audioScource.PlayOneShot(_gameOverSound);
        _gameOverMenu.SetActive(true);
        _prevScreen = _gameOverMenu;
        Debug.Log(_adPreviouslyShown);
        if (!_adPreviouslyShown)
        {
            var random = Random.Range(1, 5);
            Debug.Log(random);
            if (random == 1)
            {
                ShowAdScreen();
            }
        }
    }

    public void ShowOptionsMenu()
    {
        Debug.Log("WTF");
        _gameController.SetPause(true);
        _menuParent.SetActive(true);
        RefreshScreen();
        PlaySelectSound();
        _backButton.onClick.RemoveAllListeners();
        _backButton.onClick.AddListener(ReturnToPreviousScreen);
        _optionsMenu.SetActive(true);
    }

    private void ReturnToPreviousScreen()
    {
        RefreshScreen();
        PlaySelectSound();
        if(_prevScreen.name == "Game Screen")
        {
            _menuParent.SetActive(false);
            _gameController.SetPause(false);
        }
        _prevScreen.SetActive(true);
    }

    public void ShowGameMenu()
    {
        if (PlayGamesPlatform.Instance.IsAuthenticated())
        {
            PlayGamesPlatform.Instance.LoadScores(
            "CgkI5-6CjOMQEAIQAA",
            LeaderboardStart.PlayerCentered,
            1,
            LeaderboardCollection.Public,
            LeaderboardTimeSpan.AllTime,
        (LeaderboardScoreData data) => {
            SetHighScore((int)data.PlayerScore.value);
        });
        }
        _gameController.SetPause(false);
        RefreshScreen();
        _menuParent.SetActive(false);
        PlaySelectSound();
        _gameScreen.SetActive(true);
        _prevScreen = _gameScreen;
        PlayGameMusic();
        _gameController.NewGame(_score, _maxFill, _continueGame);
    }

    public void SetAdPreviouslyShown(bool shown)
    {
        _adPreviouslyShown = shown;
    }

    public void SetContinue(bool continueGame)
    {
        _continueGame = continueGame;
    }

    public void DisableAdScreen()
    {
        _continueGame = false;
        _adScreen.SetActive(false);
    }

    public void RefuseAd()
    {
        _continueGame = false;
    }

    public void ShowAdScreen()
    {
        _adScreen.SetActive(true);
    }

    public void PlaySelectSound()
    {
        _audioScource.PlayOneShot(_selectSound);
    }

    public void SetScoreText(string text)
    {
        _scoreText.text = text;
    }

    public void PlayMenuMusic()
    {
        _musicController.PlayMenuMusic();
    }

    public void PlayGameMusic()
    {
        _musicController.PlayGameMusic();
    }
}
