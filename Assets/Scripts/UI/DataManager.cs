﻿using UnityEngine;

public class DataManager : MonoBehaviour
{
    [SerializeField] private UIController _uiController = null;
  
    public void SaveScoreToPlayerPrefs(int highScore)
    {
        PlayerPrefs.SetInt("Score", highScore);
    }

    public int LoadScoreFromPlayerPrefs()
    {
        return PlayerPrefs.GetInt("Score", 0);
    }

    public void SaveTutorialPlayerPrefs(int tutorial)
    {
        PlayerPrefs.SetInt("Tutorial", tutorial);
    }

    public int LoadTurotialFromPlayerPrefs()
    {
        return PlayerPrefs.GetInt("Tutorial", 0);
    }
}
