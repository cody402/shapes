﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackgroundShapeController : MonoBehaviour
{
    [SerializeField] private RectTransform _transform = null;
    [SerializeField] private Vector2 _speedRange = new Vector2(10, 15);
    private float _rotateSpeed = 0;
    private float _moveSpeed;

    private void Start()
    {
        _moveSpeed = Random.Range(_speedRange.x, _speedRange.y);
        while(_rotateSpeed > -1 && _rotateSpeed < 1)
        {
            _rotateSpeed = Random.Range(-3, 3);
        }
    }

    private void Update()
    {
        _transform.Rotate(new Vector3(0, 0, .1f) * _rotateSpeed);
        _transform.Translate(new Vector3(.1f * _moveSpeed, 0, 0), Space.World);
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.transform.tag == "KillZone")
        {
            Destroy(gameObject);
        }
    }
}


