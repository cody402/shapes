﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundShapeSpawner : MonoBehaviour
{
    [SerializeField] private GameObject _shape = null;

    private void OnEnable()
    {
        StartCoroutine(StartDelay());
    }

    IEnumerator SpawnDelay()
    {
        Instantiate(_shape, new Vector3(transform.position.x, transform.position.y + Random.Range(-400,400), transform.position.z), Quaternion.identity, GameObject.FindGameObjectWithTag("Spawner").transform);
        yield return new WaitForSeconds(Random.Range(8,16));
    }

    IEnumerator StartDelay()
    {
        yield return new WaitForSeconds(Random.Range(0, 10));
        StartCoroutine(SpawnDelay());
    }
}
